# Chapter 1: Continuous Integration

Continuous Integration means that we are checking the validity of your code every time we push code to the repository.
In this chapter we are going to implement this by executing unit tests on our frontend and backend. 

This project contains a basic two-tier web application. 
The backend is contained in the `backend` folder and uses Spring Boot.
The Frontend is located in the `frontend` folder, which is written in Angular.

## Step 1: Testing our Frontend
First, we are going to execute our frontend tests! First, let's execute them locally (if possible):

```bash
cd frontend
npm install
npm test
```

Often you can run your CI-scripts locally, to test them. In this step you can use the commands above!

### Assignment
Define a job which uses the above CI script to verify the integrity of the frontend

### Resources
- https://docs.gitlab.com/ee/ci/yaml/#script

## Step 2: Failing our Frontend
We want our pipeline to fail when our tests fail. 
Let's see what that looks like. 

### Assignment
- Edit one of the angular tests (`frontend/src/app/app.component.spec.ts`) to always fail
- Commit and push it
- Look at the pipelines page on Gitlab
- Revert the change so the pipeline succeeds again

## Step 3: Testing our Backend
We also want to test our backend. We can do this using the following commands:

```bash
cd backend
mvn test
```
### Assignment
Define a job which uses the above CI script to verify the integrity of the backend

## Step 4: Running in parallel
When using CI, you generally want the result as fast as possible so you have a short feedback loop about the quality of the code.

In the previous step you added backend tests, which ran after the frontend tests. 
Was this really efficient? Why not run them at the same time?

Let's do that! Gitlab supports stages. Let's consider the following yml:

```yml
stages:
- one
- two

job_one_one:
  stage: one
  script: echo "one one"
job_one_two:
  stage: one
  script: echo "one two"
job_two_one:
  stage: two
  script: echo "two one"
job_two_two:
  stage: two
  script: echo "two two"
```

The stages array defines the order in which the stages are executed. In this case the `one` stage is executed first. 
This stage runs `job_one_one` and `job_one_two` in parallel.

After all jobs in the `one` stage succeeds, stage `two` is executed, also containing two jobs in parallel.

### Resources
- https://docs.gitlab.com/ee/ci/yaml/#stages

### Assignment
Apply the above to your configuration; make a `test` phase which runs the frontend and backend test jobs in parallel.
 

## End of Chapter
You have successfully made it through the first chapter! You can compare your results with the `results/chapter-1` branch now!
